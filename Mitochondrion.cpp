#include "Mitochondrion.h"
#include "Protein.h"
#include "AminuAcid.h"
void Mitochondrion::init()
{
	_glocuse_level = 0;
	_has_glocuse_receptor = false;
}

void Mitochondrion::insert_glucose_receptor(const Protein& protein)
{
	bool flag = false;
	AminoAcid wanted[7] = { ALANINE, LEUCINE, GLYCINE, HISTIDINE, LEUCINE, PHENYLALANINE, AMINO_CHAIN_END };
	AminoAcid check[7] = { };
	AminoAcidNode* curr = protein.get_first();
	int i = 0;
	while (curr != nullptr)
	{
		check[i] = curr->get_data();
		curr = curr->get_next();
		i++;
	}
	for (i = 0; i < 7 && !flag; i++)
	{
		if (check[i] != wanted[i])
		{
			_has_glocuse_receptor = false;
			flag = true;
		}
	}
	if (!flag)
	{
		_has_glocuse_receptor = true;
	}
}

void Mitochondrion::set_glucose(const unsigned int glocuse_units)
{
	_glocuse_level = glocuse_units;
}

bool Mitochondrion::produceATP() const
{
	bool returner = false;
	if (_has_glocuse_receptor && _glocuse_level >= 50)
	{
		returner = true;
	}
	return returner;
}