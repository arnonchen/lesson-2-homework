#include <string>
#include "Ribosome.h"
#include "Protein.h"
#include <iostream>

Protein* Ribosome::create_protein(std::string& RNA_transcript) const
{
	Protein* protein = new Protein;
	AminoAcid curr;
	protein->init();
	std::string codon = "";
	int iterations = RNA_transcript.length() / 3;
	int i = 0;
	for (i = 0; i < iterations; i++)
	{
		codon = RNA_transcript.substr(0, 3);
		RNA_transcript.erase(0, 3);
		curr = get_amino_acid(codon);
		if (curr == UNKNOWN)
		{
			protein->clear();
			protein = nullptr;
		}
		protein->add(curr);
	}
	return protein;
}