#include "Cell.h"

void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
	_glocus_receptor_gene = glucose_receptor_gene;
	_mitochondrion.init();
	_nucleus.init(dna_sequence);
}

bool Cell::get_ATP()
{
	bool returned;
	std::string rna = _nucleus.get_RNA_transcript(_glocus_receptor_gene);
	Protein* protein = _ribosome.create_protein(rna);
	if (protein == nullptr)
	{
		std::cerr << "Error, RNA is not right" << std::endl;
		exit(1);
	}
	_mitochondrion.insert_glucose_receptor(*protein);
	_mitochondrion.set_glucose(50);
	protein->clear();
	delete protein;
	if (_mitochondrion.produceATP())
	{
		_atp_units = 100;
		returned = true;
	}
	else
	{
		returned = false;
	}
	return returned;
}