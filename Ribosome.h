#pragma once
#include "Ribosome.h"
#include "Protein.h"
class Ribosome
{
public:
	Protein* create_protein(std::string& RNA_transcript) const;
};