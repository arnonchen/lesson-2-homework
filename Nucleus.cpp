#include <iostream>
#include <string>
#include "AminuAcid.h"
#include "Nucleus.h"
#include "Protein.h"
void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	_start = start;
	_end = end;
	_on_complementary_dna_strand = on_complementary_dna_strand;
}

unsigned int Gene::get_start() const
{
	return _start;
}

unsigned int Gene::get_end() const
{
	return _end;
}

unsigned int Gene::is_on_complementary_dna_strand() const
{
	return _on_complementary_dna_strand;
}

void Gene::set_start(const unsigned int start)
{
	_start = start;
}

void Gene::set_end(const unsigned int end)
{
	_end = end;
}

void Gene::set_on_complementary_dna_strand(const bool on_complementary_dna_strand)
{
	_on_complementary_dna_strand = on_complementary_dna_strand;
}

void Nucleus::init(const std::string dna_sequence)
{
	int i = 0;
	_DNA_strand = dna_sequence;
	_complementary_DNA_strand = dna_sequence;
	for (i = 0; i < _complementary_DNA_strand.length(); i++)
	{
		switch (_complementary_DNA_strand[ i ])
		{
		case 'G':
			_complementary_DNA_strand[ i ] = 'C';
			break;
		case 'C':
			_complementary_DNA_strand[i] = 'G';
			break;
		case 'A':
			_complementary_DNA_strand[i] = 'T';
			break;
		case 'T':
			_complementary_DNA_strand[i] = 'A';
			break;
		default:
			std::cerr << "Error, only A G C and T allowed in the dna sequence" << std::endl;
			exit(1);
		}
	}
}

std::string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	int i = 0;
	bool on_complementary_dna_strand = gene.is_on_complementary_dna_strand();

	std::string RNA_transcript = "";
	if (on_complementary_dna_strand)
	{
		for (i = gene.get_start(); i <= gene.get_end(); i++)
		{
			if (_complementary_DNA_strand[i] == 'T')
			{
				RNA_transcript = RNA_transcript + 'U';
			}
			else
			{
				RNA_transcript = RNA_transcript + _complementary_DNA_strand[i];
			}
		}
	}
	else
	{
		for (i = gene.get_start(); i <= gene.get_end(); i++)
		{
			if (_DNA_strand[i] == 'T')
			{
				RNA_transcript = RNA_transcript + 'U';
			}
			else
			{
				RNA_transcript = RNA_transcript + _DNA_strand[i];
			}
		}
	}	
	return RNA_transcript;
}

std::string Nucleus::get_reversed_DNA_strand() const
{
	int i = 0;
	std::string reversed_DNA = "";
	for (i = _DNA_strand.length() - 1; i >= 0; i--)
	{
		reversed_DNA = reversed_DNA + _DNA_strand[ i ];
	}
	return reversed_DNA;
}
unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const
{
	int i = 0;
	int number_of_times = 0;
	for (i = 0; i < _DNA_strand.length(); i++)
	{
		if (codon[ 0 ] == _DNA_strand[ i ] && codon[ 1 ] == _DNA_strand[ i + 1 ] && codon[ 2 ] == _DNA_strand[ i + 2 ])
		{
			number_of_times++;
		}
	}
	return number_of_times;
}